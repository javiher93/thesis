
//Program used to test GPIO
#include <wiringPi.h>

int main(void){
    
    wiringPiSetup();
    pinMode(0, OUTPUT);
    for(;;){
        digitalWrite(15, HIGH);delay(500);
        digitalWrite(15, LOW); delay(500);
    }

    return 0;
}