/*
** This code is used to control a LED using GPIO from the Raspberry Pi 3 and the Emotiv Insight
** The LED is connected to pin GPIO14 and change state according to MentalCommands
** If the power of the Mental command it's over 6.0 the LED turn off or turn on 
** According to the attached command.
** Javier A. Heredia Jiménez
*/

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <map>
#include <cstdlib>
#include <stdexcept>
#include <iomanip>

//GPIO Library
#include <wiringPi.h>

//Emotiv Libraries
#include "IEmoStateDLL.h"
#include "Iedk.h"
#include "IedkErrorCode.h"
//Local libraries
#include "MentalCommandControl.h"

//Libraries for linux
#include <unistd.h>
#include <termios.h>

using namespace std;

//Functions headers
void promptUser();
void menu();
void trainingMenu();
static bool handleUserInput();
void handleMentalCommandEvent(unsigned int userID, EmoEngineEventHandle cognitivEvent);
void handleGPIO(EmoStateHandle eState);
int _kbhit(void);
int _getch(void);

unsigned int LED = 15, LED_NEUTRAL = 16;

int main(int argc, char **argv){

    //Initialize GPIO from Raspberry Pi
    wiringPiSetup();
    pinMode(LED,OUTPUT);
    pinMode(LED_NEUTRAL, OUTPUT);
    digitalWrite(LED, LOW);
    digitalWrite(LED_NEUTRAL, LOW);

    //Event to handle the EmoEngine Events
    EmoEngineEventHandle eEvent = IEE_EmoEngineEventCreate();
    EmoStateHandle eState       = IEE_EmoStateCreate();
    unsigned int userID         = 0;

    //Try to connect
    if(IEE_EngineConnect() != EDK_OK){
        cout << "Emotiv Driver start up failed." << endl;
        return -1;
    }

    cout << "Type \"exit\" to quit, \"help\" to list available commands..." << endl;
    promptUser();
    //Main loop
    while(true){

        //If key is hit
        if(_kbhit()){
            //Handle user input
            if(!handleUserInput()){
                break;
            }
        }

        //State to handle next events if there exists and they need to be handled
        int state = IEE_EngineGetNextEvent(eEvent);
        if(state == EDK_OK){

            //Handle the type of event from the EmoEngine event
            IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
            //Get the event from the actual user
            IEE_EmoEngineEventGetUserId(eEvent, &userID);

            switch(eventType){
                case IEE_UserAdded:
                    cout << "New user " << userID << " added" << endl;
                    break;
                case IEE_UserRemoved:
                    cout << "User " << userID << " has been removed." << endl;
                    break;
                case IEE_EmoStateUpdated:
                    IEE_EmoEngineEventGetEmoState(eEvent, eState);
                    
                    //Handle GPIO
                    // digitalWrite(LED_NEUTRAL, HIGH);
                    handleGPIO(eState);

                    break;
                case IEE_MentalCommandEvent:
                    handleMentalCommandEvent(userID, eEvent);
                    break;
                case IEE_FacialExpressionEvent:
                    cout << "Facial Expression Event TBI...!" << endl;
                    break;
                default:
                    break;
            }
            

        }else if(state != EDK_NO_EVENT){
            cout << "Internal error in Emotiv Engine!" << endl;
            cout << "Press any key to exit..." << endl;
            while(!_kbhit())
            break;
        }

        //Wait 5000 microseconds
        usleep(5000);
    }

    digitalWrite(LED, LOW);
    digitalWrite(LED_NEUTRAL, LOW);

    IEE_EngineDisconnect();
    IEE_EmoStateFree(eState);
    IEE_EmoEngineEventFree(eEvent);

    return 0;
}

void promptUser(){
    cout << "MentalCommand > ";
}

static bool handleUserInput(){

    static string inputBuffer;
    char key = _getch();

    if(key == '\n'){
        cout << '\n';
        string command;

        const size_t len = inputBuffer.length();
        command.reserve(len);

        //Convert the input to lower case first
        for(size_t i=0; i<len; i++){
            command.append(1, tolower(inputBuffer.at(i)));
        }

        //We clear the inputBuffer
        inputBuffer.clear();

        bool success = parseCommand(command, cout);
        promptUser();
        return success;
    } else {
        if(key == '\b'){ //Backspace key pressed
            if(inputBuffer.length()){
                putchar(key);
                putchar(' ');
                putchar(key);
                inputBuffer.erase(inputBuffer.end()-1);
            }
        } else {
            //Put letter after letter in inputBuffer
            inputBuffer.append(1,key);
            cout << key;
        }
    }

    return true;
    
}

void handleMentalCommandEvent(unsigned int userID, EmoEngineEventHandle cognitivEvent){
    
    //Mental event type to handle the eventes related with MentalCommands
    IEE_MentalCommandEvent_t mentalEventType = IEE_MentalCommandEventGetType(cognitivEvent);

    switch(mentalEventType){
        case IEE_MentalCommandTrainingStarted:
            cout << "MentalCommand training for user " << userID << " STARTED!" << endl;
            break;
        case IEE_MentalCommandTrainingSucceeded:
            cout << "Mental Command SUCCEEDEED!" << endl;
            // cout << "Accept Training!!" << endl;
            // IEE_MentalCommandSetTrainingControl(userID, MC_ACCEPT);
            break;
        case IEE_MentalCommandTrainingFailed:
            cout << "MentalCommand training for user " << userID << " FAILED!" << endl;
            break;
        case IEE_MentalCommandTrainingCompleted:
            cout << "MentalCommand training for user " << userID << " COMPLETED!" << endl;
            break;
        case IEE_MentalCommandTrainingDataErased:
            cout << "MentalCommand training data for user " << userID << " ERASED!" << endl;
            break;
        case IEE_MentalCommandTrainingRejected:
            cout << "MentalCommand training for user " << userID << " REJECTED!" << endl;
            break;  
        case IEE_MentalCommandTrainingReset:
            cout << "MentalCommand training for user " << userID << " RESET!" << endl;
            break;
        case IEE_MentalCommandAutoSamplingNeutralCompleted:
            cout << "MentalCommand auto sampling neutral for user " << userID << " COMPLETED!" << endl;
            break;
        case IEE_MentalCommandSignatureUpdated:
            cout << "MentalCommand signature for user " << userID << " UPDATED!" << endl;
            break;
        case IEE_MentalCommandNoEvent:
            cout << "NO EVENT" << endl;
            break;
        default:
            break;
    }
}

void handleGPIO(EmoStateHandle eState){

    // cout << "Handling GPIO..." << endl;
    IEE_MentalCommandAction_t actionType = IS_MentalCommandGetCurrentAction(eState);
    float actionPower = IS_MentalCommandGetCurrentActionPower(eState);

    if(actionType == MC_NEUTRAL){
        // cout << "Power: " << (actionPower*1.0f) << endl;
        // digitalWrite(LED_NEUTRAL, HIGH);
    } else if(actionType == MC_PUSH){
        // power > 0.6 ? digitalWrite(LED,HIGH) : digitalWrite(LED,LOW);
        cout << "Turning On: " << (actionPower*1.0f) << endl;
        if(actionPower > 0.6) digitalWrite(LED, HIGH);
    }else  if(actionType == MC_PULL){
        cout << "Turning Off: " << (actionPower*1.0f) << endl;
        if(actionPower > 0.7) digitalWrite(LED,LOW);
    } else {
        // cout << "Power: " << actionPower << endl;
        digitalWrite(LED_NEUTRAL, LOW);
    }

}

//Linux functions
int _kbhit(void){
    struct timeval tv;
    fd_set read_fd;

    tv.tv_sec=0;
    tv.tv_usec=0;

    FD_ZERO(&read_fd);
    FD_SET(0,&read_fd);

    if(select(1, &read_fd,NULL, NULL, &tv) == -1)
    return 0;

    if(FD_ISSET(0,&read_fd))
        return 1;

    return 0;
}

int _getch( void ){
   struct termios oldattr, newattr;
   int ch;

   tcgetattr( STDIN_FILENO, &oldattr );
   newattr = oldattr;
   newattr.c_lflag &= ~( ICANON | ECHO );
   tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
   ch = getchar();
   tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );

   return ch;
}