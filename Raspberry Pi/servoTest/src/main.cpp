
#include <wiringPi.h>
#include <softPwm.h>

int main(void){

    wiringPiSetup();
    pinMode(0,PWM_OUTPUT);
    for(;;){
        if(softPwmCreate(0,0,100)==0){
            for(int i=0;i<100;i++){
                softPwmWrite(0,i);
                delay(100);
            }
            for(int i=100;i>0;--i){
                softPwmWrite(0,i);
                delay(100);
            }
        }
        // digitalWrite(0, HIGH);delay(500);
        // digitalWrite(0, LOW); delay(500);
    }
}