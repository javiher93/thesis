/*

*/

#include <iostream>
#include <map>
#include <sstream>
#include <cassert>
#include <stdexcept>
#include <cstdlib>
#include <stdio.h>

#include "IEmoStateDLL.h"
#include "Iedk.h"
#include "IedkErrorCode.h"
#include "MentalCommandControl.h"

//Libraries for linux
#include <unistd.h>
#include <termios.h>
int _kbhit(void);
int _getch(void);

void handleMentalCommandEvent(std::ostream& os, EmoEngineEventHandle cognitivEvent);
bool handleUserInput();
void promptUser();

int main(int argc, char** argv){
    
    EmoEngineEventHandle eEvent = IEE_EmoEngineEventCreate();
    EmoStateHandle eState       = IEE_EmoStateCreate();
    unsigned int userID         = 0;

    try{ /*Connect with EmoEngine*/
        if(IEE_EngineConnect() != EDK_OK){
            throw std::runtime_error("Emotiv Driver start up failed.");
        } else {
            std::cout << "Emotiv Driver started!" << std::endl;
        }

        std::cout << "Type \"exit\" to quit, \"help\" to list available commands..." << std::endl;
        promptUser();

        while(true){
            //Handle the user input
            if(_kbhit()){
                if(!handleUserInput()){
                    break;
                }
            }

            int state = IEE_EngineGetNextEvent(eEvent);

            //New event needs to be handled
            if(state == EDK_OK){

                IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
                IEE_EmoEngineEventGetUserId(eEvent, &userID);

                switch(eventType){
                    //New headset connected
                    case IEE_UserAdded:
                        std::cout << std::endl << "New user " << userID
                        << " added.." << std::endl;
                        promptUser();
                        break;
                    //Headset disconnected
                    case IEE_UserRemoved:
                        std::cout << std::endl << "User " << userID
                        << " has been removed." << std::endl;
                        promptUser();
                        break;
                    case IEE_EmoStateUpdated:
                        IEE_EmoEngineEventGetEmoState(eEvent, eState);
                        //Send information to GPIO(eState)
                        break;
                    //Handle MentalCommand training related event
                    case IEE_MentalCommandEvent:
                        handleMentalCommandEvent(std::cout, eEvent);
                        break;
                    default:
                        break;
                }
            } else if(state != EDK_NO_EVENT){
                std::cout << "Internal error in Emotiv Engine!" << std::endl;
                break;
            }

            usleep(15000);
        }

    } catch(const std::runtime_error &e){
        std::cerr << e.what() << std::endl;
        std::cout << "Pres any keys to exit..." << std::endl;
        getchar();
    }

    IEE_EngineDisconnect();
    IEE_EmoStateFree(eState);
    IEE_EmoEngineEventFree(eEvent);

    return 0;
}

void handleMentalCommandEvent(std::ostream& os, EmoEngineEventHandle cognitivEvent){

    unsigned int userID = 0;
    IEE_EmoEngineEventGetUserId(cognitivEvent, &userID);
    IEE_MentalCommandEvent_t eventType = IEE_MentalCommandEventGetType(cognitivEvent);

    switch(eventType){
        case IEE_MentalCommandTrainingStarted:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " STARTED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingSucceeded:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " SUCCIEEDED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingFailed:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " FAILED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingCompleted:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " COMPLETED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingDataErased:
        {
            os << std::endl << "MentalCommand training data for user " << userID
            << " ERASED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingRejected:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " REJECTED!" << std::endl;
            break;
        }

        case IEE_MentalCommandTrainingReset:
        {
            os << std::endl << "MentalCommand training for user " << userID
            << " RESET!" << std::endl;
            break;
        }

        case IEE_MentalCommandAutoSamplingNeutralCompleted:
        {
            os << std::endl << "MentalCommand auto sampling neutral for user "
            << userID << " COMPLETED!" << std::endl;
            break;
        }

        case IEE_MentalCommandSignatureUpdated:
        {
            os << std::endl << "MentalCommand signature for user " << userID
            << " UPDATED!" << std::endl;
            break;
        }

        case IEE_MentalCommandNoEvent:
            break;

        default:
            //@@ unhandled case
            assert(0);
            break;
    }
    promptUser();
}

bool handleUserInput(){
    
    static std::string inputBuffer;
    char c = _getch();

    if((c =='\n')){
        std::cout << std::endl;
        std::string command;

        const size_t len = inputBuffer.length();
        command.reserve(len);

        //Convert the input to lower case first
        for(size_t i=0; i < len; i++){
            command.append(1, tolower(inputBuffer.at(i)));
        }

        inputBuffer.clear();

        bool success = parseCommand(command, std::cout);
        promptUser();
        return success;
    } else {
        if(c == '\b'){ //Backspace key
            if(inputBuffer.length()){
                putchar(c);
                putchar(' ');
                putchar(c);
                inputBuffer.erase(inputBuffer.end()-1);
            }
        } else {
            inputBuffer.append(1, c);
            std::cout << c;
        }
    }

    return true;
}

void promptUser(){
    std::cout << "MentalCommandDemo> ";
}

//Special linux functions
int _kbhit(void){

    struct timeval tv;
    fd_set read_fd;

    tv.tv_sec=0;
    tv.tv_usec=0;

    FD_ZERO(&read_fd);
    FD_SET(0,&read_fd);

    if(select(1, &read_fd,NULL, NULL, &tv) == -1)
    return 0;

    if(FD_ISSET(0,&read_fd))
        return 1;

    return 0;
}

int _getch( void ){

   struct termios oldattr, newattr;
   int ch;

   tcgetattr( STDIN_FILENO, &oldattr );
   newattr = oldattr;
   newattr.c_lflag &= ~( ICANON | ECHO );
   tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
   ch = getchar();
   tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );

   return ch;
}


