
#include <unistd.h>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <thread>
#include <vector>

#include "Iedk.h"
#include "IedkErrorCode.h"
#include "IEmoStateDLL.h"

// #include <wiringPi.h>
// #include <softPwm.h>

float xmax = 0, ymax = 0;
unsigned long globalElapsed = 0;

double GetTickCount()
{
    struct timespec now;
    if (clock_gettime(CLOCK_MONOTONIC, &now))
      return 0;
    return now.tv_sec * 1000.0 + now.tv_nsec / 1000000.0;
}

int main(int argc, char** argv){

    EmoEngineEventHandle eEvent = IEE_EmoEngineEventCreate();
    EmoStateHandle eState = IEE_EmoStateCreate();
    unsigned int userID = -1;
    int state = 0;
    bool ready = false;
    bool readyToCollect = false;

    //Begin connection with headset
    if(IEE_EngineConnect() != EDK_OK){
        std::cout << "Emotiv Drive start up failed" << std::endl;
        return -1;
    }

    std::cout << "Please keep headset on, and don't move on" << std::endl;

    IEE_MotionDataSetBufferSizeInSec(1);

    while(true){

        state = IEE_EngineGetNextEvent(eEvent);
        if(state == EDK_OK){
            IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
            IEE_EmoEngineEventGetUserId(eEvent, &userID);

            if(eventType == IEE_UserAdded){
                std::cout << "New user added" << std::endl;
                ready = true;
            }
        }

        if(!ready) continue;

        int gyroX = 0, gyroY = 0;
        int err = IEE_HeadsetGetGyroDelta(userID, &gyroX, &gyroY);

        if(err == EDK_OK){
            std::cout << "Move your head now" << std::endl;
            usleep(10000);
            readyToCollect = true;
            // break;
        }else if(err = EDK_GYRO_NOT_CALIBRATED){
            std::cout << "Gyro is not calibrated. Please stay still." << std::endl;
            usleep(10000);
        }else if(err == EDK_CANNOT_ACQUIRE_DATA){
            std::cout << "Cannot acquire data" << std::endl;
        }else{
            std::cout << "No headset is connected" << std::endl;
        }

        if(readyToCollect){
            xmax += gyroX;
            ymax += gyroY;
            std::cout << "xmax: " << xmax << " ; ymax: " << ymax << std::endl;
            usleep(10000);
        }else{

        }

        usleep(10000);
    }

    globalElapsed = (unsigned long)GetTickCount();

    IEE_EngineDisconnect();
    IEE_EmoStateFree(eState);
    IEE_EmoEngineEventFree(eEvent);

    return 0;
}

