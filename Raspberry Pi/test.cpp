#include <stdio.h>
#include <iostream>
#include <sstream>

#include <unistd.h>
#include <termios.h>

// int _kbhit(void);
// int _getch( void );

using namespace std;

int main(int argc, char **argv){
    
    char op;

    // cout << "Hello world!" << endl;
    // cout << "Introduce something: " ;
    // cin >> op;
    // cout << "You write : " << op << endl;

    cout << "=====================================================================================" << endl;
    cout << "Please choose one option." << endl;
    cout << "Press 1 to Active training session." << endl;
    cout << "Press 2 to show Power control from mental commands (this require training session)." << endl;
    cout << "Press 3 to enter GPIO menu."<< endl;
    cout << "Press 0 to help" << endl;
    cout << "Press ` to quit." << endl;
    cout << "======================================================================================" << endl;

    cin >> op;

    while(true){

        switch(op){
            case '`':
                cout << "Bye..."
                break;
            default:
                cout << "Option wrong..."
                break;
        }

    }

    return -1;
}

//Linux functions
// int _kbhit(void){
//     struct timeval tv;
//     fd_set read_fd;

//     tv.tv_sec=0;
//     tv.tv_usec=0;

//     FD_ZERO(&read_fd);
//     FD_SET(0,&read_fd);

//     if(select(1, &read_fd,NULL, NULL, &tv) == -1)
//     return 0;

//     if(FD_ISSET(0,&read_fd))
//         return 1;

//     return 0;
// }

// int _getch( void ){
//    struct termios oldattr, newattr;
//    int ch;

//    tcgetattr( STDIN_FILENO, &oldattr );
//    newattr = oldattr;
//    newattr.c_lflag &= ~( ICANON | ECHO );
//    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
//    ch = getchar();
//    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );

//    return ch;
// }
