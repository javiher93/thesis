#Set minimum required version 
cmake_minimum_required(VERSION 3.0.0)

#Name the project
project(gyro)

#Create variables
set(GYRO_EXE Gyro)
file(GLOB SOURCES "src/*.cpp")

#We're developing under Raspbian Linux
#We have to stablish the binary and include files 
# set(PROJECT_LIB_DIR ${PROJECT_SOURCE_DIR}/lib/libedk.so.3.3.3)
set(PROJECT_LIB_DIR "/home/pi/Documents/Thesis/Emotiv/lib/libedk.so.3.3.3")
set(PROJECT_INCLUDE_DIR "/home/pi/Documents/Thesis/Emotiv/include")
# set(PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
# set(PROJECT_LINK_LIBS edk.so.3.3.3)
# link_directories(PROJECT_LIB_DIR)

# add_library(edk SHARED IMPORTED)
add_library(libedk.so.3.3.3 SHARED IMPORTED)
set_property(TARGET libedk.so.3.3.3 PROPERTY IMPORTED_LOCATION ${PROJECT_LIB_DIR})
#"~/Documents/Thesis/Emotiv/GyroData/lib/edk.so.3.3.3")
# message(STATUS "Bin dir: " ${PROJECT_BIN_DIR})
# message(STATUS "include dir: " ${PROJECT_INCLUDE_DIR})
# message(STATUS "Lib dir: " ${PROJECT_LINK_LIBS})

#We create the executable and link libraries
add_executable(${GYRO_EXE} ${SOURCES})
target_link_libraries(${GYRO_EXE} libedk.so.3.3.3)
target_include_directories(${GYRO_EXE} PRIVATE ${PROJECT_INCLUDE_DIR})

#That's all

