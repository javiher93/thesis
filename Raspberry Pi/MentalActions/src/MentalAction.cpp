/****************************************************************************
**
**
****************************************************************************/

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <thread>

//Linux libraries
#include <unistd.h>
#include <termios.h>

#include "IEmoStateDLL.h"
#include "Iedk.h"
#include "IedkErrorCode.h"
#include "EmotivCloudClient.h"
#include "MentalCommandDetection.h"

#include <wiringPi.h>

int _kbhit(void);
int _getch( void );

const std::string userName = "061093";
const std::string password = "21B0E11c57";
const std::string profileName = "Javier";

unsigned int userID = 0;
int userCloudID = -1;
bool enableLoger = false;

int version	= -1; // Lastest version
bool running = true;
typedef unsigned long ulong;

float skill = 0;

void help();

void SavingLoadingFunction(int userCloudID, bool save = true){

	int getNumberProfile = EC_GetAllProfileName(userCloudID);
	
	if (save) {    // Save profile to cloud
        int profileID = -1;
        EC_GetProfileId(userCloudID, profileName.c_str(), &profileID);

		if (profileID >= 0) {
			std::cout << "Profile with " << profileName << " is existed." << std::endl;
            if (EC_UpdateUserProfile(userCloudID, userID, profileID) == EDK_OK)
				std::cout << "Updating finished" << std::endl;
			else 
				std::cout << "Updating failed" << std::endl;
		}
        else if (EC_SaveUserProfile(userCloudID, userID, profileName.c_str(), TRAINING) == EDK_OK)
		{
			std::cout << "Saving finished" << std::endl;
		}
		else std::cout << "Saving failed" << std::endl;
		return;
	} else { // Load profile from cloud
		if (getNumberProfile > 0){
            if (EC_LoadUserProfile(userCloudID, userID, EC_ProfileIDAtIndex(userCloudID, 0)) == EDK_OK)
				std::cout << "Loading finished" << std::endl;
			else
				std::cout << "Loading failed" << std::endl;

		}
		return;
	}
}

void handleMentalCommandEvent(unsigned int userID, EmoEngineEventHandle cognitivEvent){

    IEE_MentalCommandEvent_t eventType = IEE_MentalCommandEventGetType(cognitivEvent);

	switch (eventType) {

    case IEE_MentalCommandTrainingStarted:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " STARTED!" << std::endl;
        break;
    }

    case IEE_MentalCommandTrainingSucceeded:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " SUCCEEDED!" << std::endl;		
		std::cout << "Accept Training!!!" << std::endl;
		IEE_MentalCommandSetTrainingControl(userID, MC_ACCEPT);
		// Reject Training:
		// IEE_MentalCommandSetTrainingControl(userID, MC_REJECT);
        break;
    }

    case IEE_MentalCommandTrainingFailed:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " FAILED!" << std::endl;
        break;
    }

    case IEE_MentalCommandTrainingCompleted:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " COMPLETED!" << std::endl;
        break;
    }

    case IEE_MentalCommandTrainingDataErased:
    {
        std::cout<< std::endl << "MentalCommand training data for user " << userID
           << " ERASED!" << std::endl;
        break;
    }

    case IEE_MentalCommandTrainingRejected:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " REJECTED!" << std::endl;
        break;
    }

    case IEE_MentalCommandTrainingReset:
    {
        std::cout<< std::endl << "MentalCommand training for user " << userID
           << " RESET!" << std::endl;
        break;
    }

    case IEE_MentalCommandAutoSamplingNeutralCompleted:
    {
        std::cout<< std::endl << "MentalCommand auto sampling neutral for user "
           << userID << " COMPLETED!" << std::endl;
        break;
    }

    case IEE_MentalCommandSignatureUpdated:
    {
        std::cout << std::endl << "MentalCommand signature for user " << userID
           << " UPDATED!" << std::endl;
        break;
    }

    case IEE_MentalCommandNoEvent:
        break;

    default:
        break;
	}
}

static bool handleUserInput(){

	static std::string inputBuffer;
	char key = _getch();

	switch (key)
	{
	case '1':
	{
        int getNumberProfile = EC_GetAllProfileName(userCloudID);

        int profileOK = false;

        int profileID = -1;
        EC_GetProfileId(userCloudID, profileName.c_str(), &profileID);

        if (profileID >= 0) {
            if (EC_LoadUserProfile(userCloudID, userID, profileID) == EDK_OK) {
                profileOK = true;
            }
        }
        if (profileOK == false) {
            if (EC_SaveUserProfile(userCloudID, userID, profileName.c_str(), TRAINING) == EDK_OK)
            {
                std::cout << "Saving finished" << std::endl;
            }
            else std::cout << "Saving failed" << std::endl;
        }

		//Mental commands to train
		ulong action1 = (ulong)IEE_MentalCommandAction_t::MC_PULL;
		ulong action2 = (ulong)IEE_MentalCommandAction_t::MC_PUSH;
		//Variable useful for set the active actions
		ulong listAction = action1 | action2;

		int errorCode = EDK_OK;
		errorCode = IEE_MentalCommandSetActiveActions(userID, listAction);
		if (errorCode == EDK_OK)	//if is successful
		{
			std::cout << "Setting MentalCommand active actions (MC_PULL | MC_PUSH) for user " << userID << std::endl;			
		}
		else std::cout << "Setting MentalCommand active actions error: " << errorCode;
	}
	break;
	case '2':
		std::cout << std::endl << "Starting training NEUTRAL !" << std::endl;
		//Set the type of MentalCommand action to be trained.
		IEE_MentalCommandSetTrainingAction(userID, MC_NEUTRAL);
		//Set the training control flag for MentalCommand training.
		IEE_MentalCommandSetTrainingControl(userID, MC_START);
		break;
	case '3':
		std::cout << std::endl << "Starting training RIGHT !" << std::endl;
		int errorCode;
		//Set the type of MentalCommand action to be trained.
		errorCode = IEE_MentalCommandSetTrainingAction(userID, MC_PUSH);
		// Set the training control flag for MentalCommand training.
		errorCode = IEE_MentalCommandSetTrainingControl(userID, MC_START);
		break;
	case '4':
		std::cout << std::endl << "Starting training LEFT !" << std::endl;
		//Set the type of MentalCommand action to be trained.
		IEE_MentalCommandSetTrainingAction(userID, MC_PULL);
		// Set the training control flag for MentalCommand training.
		IEE_MentalCommandSetTrainingControl(userID, MC_START);
		break;
	case '5':
	{
		int level = 0;
		// Set the overall sensitivity for all MentalCommand actions to 2
		//sensitivity level of all actions (lowest: 1, highest: 7)
		if (IEE_MentalCommandSetActivationLevel(userID, 2) == EDK_OK) {
			//Get the overall sensitivity for all MentalCommand actions.
			//sensitivity level of all actions (min: 1, max: 10)
			IEE_MentalCommandGetActivationLevel(userID, &level);
			std::cout << "Set MentalCommand Activation level to " << level << std::endl;
		}
		else {
			std::cout << "Set MentalCommand Activation level failured " << level << std::endl;
		}
	}
	break;
	case '6':
	{
		int level = 0;
		//Get the overall sensitivity for all MentalCommand actions.
		//sensitivity level of all actions (min: 1, max: 10)
		IEE_MentalCommandGetActivationLevel(userID, &level);
		std::cout << "Current MentalCommand Activation level: " << level << std::endl;
	}
		break;
	case '7':
	{
		// float skill = 0;
		// Get the current overall skill rating of the user in MentalCommand.
		// receives the overall skill rating [from 0.0 to 1.0]
		IEE_MentalCommandGetOverallSkillRating(userID, &skill);
		std::cout << "Current overall skill rating: " << skill << std::endl;
	}
	break;
	case '8':
		std::cout << "Saving Profile ... " <<  std::endl;
		SavingLoadingFunction(userCloudID, true);
		break;
	case '9':
		std::cout << "Loading Profile ... " << std::endl;
		SavingLoadingFunction(userCloudID, false);
		break;
	case '0':
		help();
		break;
	case '`':
		return false;
		break;
	default:
		break;
	}
	return true;
}

void handleGPIO(EmoStateHandle eState){

	IEE_MentalCommandAction_t actionType = IS_MentalCommandGetCurrentAction(eState);
	float actionPower = IS_MentalCommandGetCurrentActionPower(eState);

	if(skill > 2.0){
		std::cout << actionType << "," << (actionPower*100.0f) << std::endl;
	}
	
}

int main(int argc, char** argv){
	
	int option	= 0;
	std::string input;
	bool ready  = false;

	EmoEngineEventHandle eEvent	= IEE_EmoEngineEventCreate();
	EmoStateHandle eState		= IEE_EmoStateCreate();
	
	int state			        = 0;		
		
	if (IEE_EngineConnect() != EDK_OK) {
        std::cout << "Emotiv Driver start up failed.";
        return -1;
	}

	std::cout << "===================================================================" << std::endl;
    std::cout << "Example to show how to detect mental command detection with a profile. " << std::endl;
    std::cout << "===================================================================" << std::endl;
	std::cout << "Connecting to cloud ..." << std::endl;

    if(EC_Connect() != EDK_OK)
	{
		std::cout << "Cannot connect to Emotiv Cloud";
        return -1;
	}

    if(EC_Login(userName.c_str(), password.c_str()) != EDK_OK)
	{			
		std::cout << "Your login attempt has failed. The username or password may be incorrect" << std::endl;
        return -1;
	}

	std::cout<<"Logged in as " << userName << std::endl;

    if (EC_GetUserDetail(&userCloudID) != EDK_OK) {
        std::cout << "Error: Cannot get detail info, exit " << userName << std::endl;
        return -1;
    }

	help();

	while (true) {
		
		if (_kbhit()) {
			if (!handleUserInput()) {
				break;
			}
		}

		int state = IEE_EngineGetNextEvent(eEvent);

		if (state == EDK_OK) {

			IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
			IEE_EmoEngineEventGetUserId(eEvent, &userID);

			switch (eventType) 
			{
			    case IEE_UserAdded: 
					std::cout << std::endl << "New user " << userID << " added" << std::endl;
					break;
				case IEE_UserRemoved:
					std::cout << std::endl << "User " << userID << " has been removed." << std::endl;
					break;
				case IEE_EmoStateUpdated:
					IEE_EmoEngineEventGetEmoState(eEvent, eState);

					//Update GPIO
					// handleGPIO(eState);

					break;
				case IEE_MentalCommandEvent:
					handleMentalCommandEvent(userID, eEvent);
				case IEE_FacialExpressionEvent:

					break;
				default:
					break;
			}
		}
		else if (state != EDK_NO_EVENT) {
			std::cout << "Internal error in Emotiv Engine!" << std::endl;
			break;
		}

        usleep(5000);
	}
    
	IEE_EngineDisconnect();
	IEE_EmoStateFree(eState);
	IEE_EmoEngineEventFree(eEvent);
    return -1;
}

void help(){

    std::cout << "===================================================================" << std::endl;
    std::cout << "Please do follow steps:" << std::endl;
    std::cout << "A. Press 1 to Active training session." << std::endl;
    std::cout << "B. Press 2 to Start training NEUTRAL. When completed, press 3 ." << std::endl;
    std::cout << "C. Press 3 to Start training PUSH. When completed, press 4 ." << std::endl;
    std::cout << "D. Press 4 to Start training PULL." << std::endl;
    std::cout << "E. Press 5 to Set ActivationLevel." << std::endl;
    std::cout << "===================================================================" << std::endl;
    std::cout << "Otherwise, press key below for more: " << std::endl;
    std::cout << std::endl;
    std::cout << "Press 6 to Read ActivationLevel." << std::endl;
    std::cout << "Press 7 to Get OverallSkillRating." << std::endl;
    std::cout << "Press 8 to Save Profile Data to cloud." << std::endl;
    std::cout << "Press 9 to Load Profile Data from cloud." << std::endl;
    std::cout << std::endl;
    std::cout << "Press 0 for help." << std::endl;
    std::cout << "Press ` to quit." << std::endl;
    std::cout << "===================================================================" << std::endl;
}

//Linux functions
int _kbhit(void){

    struct timeval tv;
    fd_set read_fd;

    tv.tv_sec=0;
    tv.tv_usec=0;

    FD_ZERO(&read_fd);
    FD_SET(0,&read_fd);

    if(select(1, &read_fd,NULL, NULL, &tv) == -1)
    return 0;

    if(FD_ISSET(0,&read_fd))
        return 1;

    return 0;
}

int _getch(void){

	struct termios oldattr, newattr;
	int ch;

	tcgetattr( STDIN_FILENO, &oldattr );
	newattr = oldattr;
	newattr.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );

	return ch;
}

