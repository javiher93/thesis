#Set minimum version
cmake_minimum_required(VERSION 3.0.0)

#Name the project
project (motionData)

#Create variable  as MOTION_EXE
set(MOTION_EXE Motion)
file(GLOB SOURCES "src/*.cpp")

#Path to the include files
set(PROJECT_INCLUDE_DIR "/Users/javiher/Documents/Thesis/CMake Examples/include")

#We are developing under Mac OSX we need to search the framworks
#Set the variables for the libraries
set(PROJECT_LINK_LIBS "-framework edk")

#Create the executable
add_executable(Motion ${SOURCES})

#Link the libraries
target_link_libraries(${MOTION_EXE} ${PROJECT_LINK_LIBS})

#If you have extra include files "XXX.h" uncomment next line
#include_directories(include)
target_include_directories(${MOTION_EXE} PRIVATE ${PROJECT_INCLUDE_DIR})

#Search frameworks in the macOS
set_target_properties(${MOTION_EXE} PROPERTIES LINK_FLAGS "-F/Library/Frameworks")