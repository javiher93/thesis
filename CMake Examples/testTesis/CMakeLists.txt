#Set minimum version
cmake_minimum_required(VERSION 3.0.0)

#Name the project
project(gyro)

#Create variables
set(GYRO_EXE Gyro)
set(FrameworkMac)
file(GLOB SOURCES "src/*.cpp")

#As we are working in Mac OS
if(APPLE)
    #Set some variables to reference framweorks
    set(PROJECT_LINK_LIBS "-framework edk")
    set(FrameworkMac "-framework OpenGL"
                     "-framework GLUT")
endif()

#Create the executable
add_executable(Gyro ${SOURCES})
# add_executable(${GYRO_EXE} "src/main.cpp")

#Link the libraries
include_directories(include)
target_link_libraries(${GYRO_EXE} PRIVATE 
                      ${PROJECT_LINK_LIBS}
                      ${GLUT_LIBS} ${FrameworkMac})
# target_include_directories(${GYRO_EXE} PRIVATE ${COMMUNITY_SDK_INCLUDE_DIR})
#Search the frameworks in mac
if(APPLE)
    set_target_properties(${GYRO_EXE} PROPERTIES LINK_FLAGS "-F/Library/Frameworks")
endif()