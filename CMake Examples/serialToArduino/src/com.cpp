
#include <stdio.h>    // Standard input/output definitions 
#include <unistd.h>   // UNIX standard function definitions 
#include <fcntl.h>    // File control definitions 
#include <errno.h>    // Error number definitions 
#include <termios.h>  // POSIX terminal control definitions 
#include <string.h>   // String function definitions 
#include <sys/ioctl.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdint.h>

// #include "arduino-serial.h"

int serialport_init(const char* serialport, int baud){

    struct termios toptions;
    int fd;

    fd = open(serialport, O_RDWR | O_NONBLOCK);
    if(fd == -1){
        printf("serialport_init: Unable to open port \n");
        return -1;
    }

    if(tcgetattr(fd, &toptions) < 0){
        printf("serialport_init: Couldn't get term attributes \n");
        return -1;
    }

    speed_t brate = baud;
    switch(baud) {
        case 4800:   brate=B4800;   break;
        case 9600:   brate=B9600;   break;
#ifdef B14400
        case 14400:  brate=B14400;  break;
#endif
        case 19200:  brate=B19200;  break;
#ifdef B28800
        case 28800:  brate=B28800;  break;
#endif
        case 38400:  brate=B38400;  break;
        case 57600:  brate=B57600;  break;
        case 115200: brate=B115200; break;
    }

    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    //toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 0;
    //toptions.c_cc[VTIME] = 20;
    
    tcsetattr(fd, TCSANOW, &toptions);
    if( tcsetattr(fd, TCSAFLUSH, &toptions) < 0) {
        printf("init_serialport: Couldn't set term attributes \n");
        return -1;
    }

    return fd;
}

int serialport_close(int fd){
    return close(fd);
}

int serialport_writebyte(int fd, uint8_t b){
    int n = write(fd,&b,1);
    if(n!=1) return -1;

    return 0;
}

int serialport_write(int fd, const char* str){
    int len = strlen(str);
    int n = write(fd,str,len);
    if(n != len){
        printf("serialport_write: couldn't write whole string\n");
        return -1;
    }

    return 0;
}

int serialport_read_until(int fd, char* buf, char until, int buf_max, int timeout){

    char b[1];
    int i=0;
    do{
        int n = read(fd, b, 1);
        if(n == -1) return -1;
        if(n == 0){
            usleep(1*1000);
            timeout--;
            if(timeout == 0) return -2;
            continue;
        }

        printf("serialport_read_until: i=%d, n=%d, b='%c'\n",i,n,b[0]);

        buf[i] = b[0];
        i++;

    }while(b[0] != until && i < buf_max && timeout > 0);

    buf[i] = 0;
    return 0;
}

int serialport_flush(int fd){
    sleep(2);
    return tcflush(fd, TCIOFLUSH);
}

int main(int argc, char* argv[]){

    const int buf_max = 256;

    int fd = -1; //File descriptor
    const char *serialport = "/dev/tty.usbserial-DA017VTI";
    int baudrate = 9600;
    char quiet = 0;
    char eolchar = '\n';
    int timeout = 5000;
    char buf[buf_max];
    int rc, n;
    
    //Opening port
    if(fd!=-1){
        serialport_close(fd);
        if(!quiet) printf("closed port %s\n", serialport);
    }

    fd = serialport_init(serialport, baudrate);
    if(!quiet) printf("opened port %s\n", serialport);
    serialport_flush(fd);

    //Writing to port
    // char dataToSend[] = "1";
    sprintf(buf, "%s", "1");
    if(!quiet) printf("send string:%s\n", buf);
    rc = serialport_write(fd, buf);
    
    //Reading port
    memset(buf,0,buf_max);
    serialport_read_until(fd,buf,eolchar,buf_max,timeout);
    if(!quiet) printf("read string:");
    printf("%s\n", buf);

    //Closing port
    serialport_close(fd);
    
    return 0;
}