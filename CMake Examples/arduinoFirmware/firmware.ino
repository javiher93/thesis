
#include <Servo.h>

Servo baseMotor;
Servo wristMotor;
Servo handleMotor;

// Package data 238, 90, 90, 1, 163
// Header, baseValue, wristValue, handleValue, checksum
// 0xEE, 0x5A, 0x5A, 0x01, 0xA3
const uint8_t header = 0xEE;
const uint8_t bufferSize = 5;
uint8_t buffer[bufferSize];
uint8_t readCounter;
uint8_t isHeader;
uint8_t firstTimeHeader;

// uint8_t oldBase = 0, oldWrist = 0;
uint8_t base = 90, wrist = 90, closeValue = 100, openValue = 43;
//Handle value close ~ 95 Open ~43

void setup(){
    while(!Serial);
    Serial.begin(9600);

    readCounter = 0;
    isHeader = 0;
    firstTimeHeader = 0;
    
    baseMotor.attach(9);
    wristMotor.attach(10);
    handleMotor.attach(11);

    baseMotor.write(90);
    wristMotor.write(90);
    handleMotor.write(openValue);

    pinMode(13,OUTPUT);
}

void loop(){

    if(Serial.available() > 0){
    
        uint8_t c = Serial.read();

        if(c == header){

            if(!firstTimeHeader){
                isHeader = 1;
                readCounter = 0;
                firstTimeHeader = 1;
            }
        }

        buffer[readCounter] = c;
        readCounter++;

        if(readCounter >= bufferSize){
            readCounter = 0;

            if(isHeader){
                uint8_t checksumValue = buffer[4];
                if(verifyChecksum(checksumValue)){
                    //Do your thing
                    //Debug
                    digitalWrite(13,HIGH);
                    controlRobotArm(buffer[1],buffer[2],buffer[3]);
                }

                isHeader = 0;
                firstTimeHeader = 0;
            }
        }
    }

}

uint8_t verifyChecksum(uint8_t originalResult){
    uint8_t result = 0;
    uint16_t sum = 0;

    for(uint8_t i=0; i<(bufferSize - 1); i++){
        sum += buffer[i];
    }

    result = sum & 0xFF;

    if(originalResult == result){
        return 1;
    }else{
        return 0;
    }

}

void controlRobotArm(uint8_t baseValue, uint8_t wristValue, uint8_t handleValue){

    if(baseValue == 1){
        if(base <= 170) base ++;
    }else if(baseValue == 0){
        if(base >= 10) base --;
    }else if(baseValue == 0x5A){
        baseMotor.write(90);    
    }else{

    }

    if(wristValue == 1){
        if(wrist <= 170) wrist++;
    }else if(wristValue == 0){
        if(wrist >= 10) wrist --;
    }else if(wristValue == 0x5A){
        wristMotor.write(90);
    }
    
    //Write values
    baseMotor.write(base);
    wristMotor.write(wrist);
    if(handleValue == 1){
        handleMotor.write(closeValue);
    }else if(handleValue == 0){
        handleMotor.write(openValue);
    }else{
        handleMotor.write(95);
    }

}