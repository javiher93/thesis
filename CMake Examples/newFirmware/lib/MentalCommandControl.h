/* Include file for the MentalCommandControl.cpp */

#ifndef MCNITIV_CONTROL_H
#define MCNITIV_CONTROL_H

#include <string>
#include <iostream>
#include <sstream>
#include <vector>

extern bool setGyro;
//Variable used for training mental commands if 0x00 training Open if 0x11 training close
extern uint8_t training;
bool parseCommand(const std::string &command, std::ostream &output);

#endif //MCNITIV_CONTROL_H