//Standard libraries
#include <iostream>
#include <map>
#include <sstream>
#include <cassert>
#include <stdexcept>
#include <cstdlib>
#include <stdio.h>
//Emotiv Libraries
#include "IEmoStateDLL.h"
#include "Iedk.h"
#include "IedkErrorCode.h"
//Local libraries
#include "MentalCommandControl.h"
#include "arduino-serial.h"
//Apple libraries
#include <unistd.h>
#include <termios.h>
//Apple functions
int _kbhit(void);
int _getch(void);

using namespace std;

//Functions headers
void promptUser();
static bool handleUserInput();
void handleMentalCommandEvent(unsigned int userID, EmoEngineEventHandle cognitivEvent);
void getMentalData(EmoStateHandle eState);
void handleArduino(uint8_t baseValue, uint8_t wristValue, uint8_t handleValue);
uint8_t checksum();

void sendOpen();
void sendClose();

//Data to handling serial data to arduino
const int buf_max = 256;

int fd = -1; //File descriptor
const char *serialport = "/dev/tty.usbserial-DA017VTI";
int baudrate = 9600;
char quiet = 0;
char eolchar = '\n';
int timeout = 5000;
char buf[buf_max];
int rc, n;

//Data for Gyro and MentalCommands
int gyroX = 0, gyroY = 0;
float xmax = 0, ymax = 0;
bool setGyro = false;
uint8_t training = 0xFF;

//Data for package
const uint8_t packageSize = 5;
uint8_t package[packageSize];
uint8_t handle = 0;

int main(int argc, char ** argv){

    //Event handlers for EmoEngine Events
    EmoEngineEventHandle eEvent = IEE_EmoEngineEventCreate();
    EmoStateHandle eState = IEE_EmoStateCreate();
    unsigned int userID = 0;

    // Opening port of Arduino
    if(fd!=-1){
        serialport_close(fd);
        if(!quiet) printf("closed port %s\n", serialport);
    }

    fd = serialport_init(serialport, baudrate);
    if(!quiet) printf("opened port %s\n", serialport);
    serialport_flush(fd);

    //Try to connect
    if(IEE_EngineConnect() != EDK_OK){
        cout << "Emotiv Driver start up failed" << endl;
        return -1;
    }

    //Set motionData buffer for gyro data
    // IEE_MotionDataSetBufferSizeInSec(1);
    
    //Display menu
    cout << "Type \"exit\" to quit, \"help\" to list available commands..." << endl;
    promptUser();
    
    //Main loop
    while(true){
        if(_kbhit()){ //Keyboard hit
            if(!handleUserInput()){
                break;
            }
        }

        //Handle comming events
        int state = IEE_EngineGetNextEvent(eEvent);
        if(state == EDK_OK){
            //Handle the type of event
            IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
            //Get user from event
            IEE_EmoEngineEventGetUserId(eEvent, &userID);

            switch(eventType){
                case IEE_UserAdded:
                    cout << "New user " << userID << " added" << endl;
                    promptUser();
                    // ready = true;
                    break;
                case IEE_UserRemoved:
                    cout << "User " << userID << " has been removed." << endl;
                    promptUser();
                    // ready = false;
                    break;
                case IEE_EmoStateUpdated:
                    IEE_EmoEngineEventGetEmoState(eEvent, eState);
                    getMentalData(eState);
                    // isMental = true;

                    break;
                case IEE_MentalCommandEvent:
                    handleMentalCommandEvent(userID, eEvent);
                     break;
                case IEE_FacialExpressionEvent:
                    cout << "Facial Expression Event TBI...!" << endl;
                    break;
                default:
                    break;
            }
        } else if(state != EDK_NO_EVENT){
            cout << "Internal error in Emotiv Engine!" << endl;
            cout << "Press any key to exit..." << endl;
            while(!_kbhit())
            break;
        }

        if(training == 0x00){
            sendClose();
        } else if(training == 0x11){
            sendOpen();
        }else{
            
        }
        
        if(setGyro){// && setMental){
            int err = IEE_HeadsetGetGyroDelta(userID, &gyroX, &gyroY);
            if(err == EDK_OK){
                // xmax += gyroX;
                // ymax += gyroY;
                // cout << "xmax: " << xmax << " ; ymax: " << ymax << endl;
                // cout << "xmax: " << gyroX << " ; ymax: " << gyroY << endl;
                uint8_t base, wrist;
                
                // while(!isMental);

                if(gyroX > 0){
                    base = 1;
                }else if(gyroX < 0){
                    base = 0;
                }else{
                    base = 2;
                }

                if(gyroY > 0){
                    wrist = 1;
                }else if(gyroY < 0){
                    wrist = 0;
                }else{
                    wrist = 2;
                }

                // printf("Base: %d Wrist: %d \n", base, wrist);
                handleArduino(base,wrist,handle);
                usleep(45000);
            }
        }
        // isMental = false;
        usleep(5000);
    }

    //Close port of Arduino 
    serialport_close(fd);
    //Disconnecting EmoEngine and free States and Events
    IEE_EngineDisconnect();
    IEE_EmoStateFree(eState);
    IEE_EmoEngineEventFree(eEvent);

    return 0;
}

void promptUser(){
    cout << "MentalCommand > ";
}

static bool handleUserInput(){

    static string inputBuffer;
    char key = _getch();

    if(key=='\n'){
        cout << endl;
        string command;

        const size_t len = inputBuffer.length();
        command.reserve(len);

        //Convert the input to lower case first
        for(size_t i=0;i<len;i++){
            command.append(1, tolower(inputBuffer.at(i)));
        }

        //We clear the inputBuffer
        inputBuffer.clear();

        bool success = parseCommand(command, cout);
        promptUser();

        return success;
    } else {
        if(key == '\b'){ //Backspace key pressed
            if(inputBuffer.length()){
                putchar(key);
                putchar(' ');
                putchar(key);
                inputBuffer.erase(inputBuffer.end()-1);
            }
        } else {
            //Put letter after letter in inputBuffer
            inputBuffer.append(1,key);
            cout << key;
        }
    }
    return true;
}

void handleMentalCommandEvent(unsigned int userID, EmoEngineEventHandle cognitivEvent){

    //Mental event type to handle the eventes related with MentalCommands
    IEE_MentalCommandEvent_t mentalEventType = IEE_MentalCommandEventGetType(cognitivEvent);
    
        switch(mentalEventType){
            case IEE_MentalCommandTrainingStarted:
                cout << "MentalCommand training for user " << userID << " STARTED!" << endl;
                break;
            case IEE_MentalCommandTrainingSucceeded:
                cout << "Mental Command SUCCEEDEED!" << endl;
                // cout << "Accept Training!!" << endl;
                // IEE_MentalCommandSetTrainingControl(userID, MC_ACCEPT);
                break;
            case IEE_MentalCommandTrainingFailed:
                cout << "MentalCommand training for user " << userID << " FAILED!" << endl;
                break;
            case IEE_MentalCommandTrainingCompleted:
                cout << "MentalCommand training for user " << userID << " COMPLETED!" << endl;
                break;
            case IEE_MentalCommandTrainingDataErased:
                cout << "MentalCommand training data for user " << userID << " ERASED!" << endl;
                break;
            case IEE_MentalCommandTrainingRejected:
                cout << "MentalCommand training for user " << userID << " REJECTED!" << endl;
                break;  
            case IEE_MentalCommandTrainingReset:
                cout << "MentalCommand training for user " << userID << " RESET!" << endl;
                break;
            case IEE_MentalCommandAutoSamplingNeutralCompleted:
                cout << "MentalCommand auto sampling neutral for user " << userID << " COMPLETED!" << endl;
                break;
            case IEE_MentalCommandSignatureUpdated:
                cout << "MentalCommand signature for user " << userID << " UPDATED!" << endl;
                break;
            case IEE_MentalCommandNoEvent:
                cout << "NO EVENT" << endl;
                break;
            default:
                break;
        }
}

void getMentalData(EmoStateHandle eState){
    
    IEE_MentalCommandAction_t actionType = IS_MentalCommandGetCurrentAction(eState);
    float actionPower = IS_MentalCommandGetCurrentActionPower(eState);

    if(actionType == MC_NEUTRAL){
        // cout << "MC_NEUTRAL: " << (actionPower*1.0f) << endl;
    }else if(actionType == MC_PUSH){
        // cout << "MC_PUSH: " << (actionPower*1.0f) << endl;
        if(actionPower > 0.6){
            handle = 1;
            cout << "MC_PUSH: " << (actionPower*1.0f) << endl;
        }

    }else if(actionType == MC_PULL){
        // cout << "MC_PULL: " << (actionPower*1.0f) << endl;
        if(actionPower > 0.6){
            handle = 0;
            cout << "MC_PULL: " << (actionPower*1.0f) << endl;
        }

    }else{

    }
}

void sendOpen(){
    package[0] = 0xEE;
    package[1] = 0x5A;
    package[2] = 0x5A;
    package[3] = 0x00;
    package[4] = 0xA2;

    for(uint8_t i=0; i < packageSize; i++){
        serialport_writebyte(fd, package[i]);
    }
}

void sendClose(){
    package[0] = 0xEE;
    package[1] = 0x5A;
    package[2] = 0x5A;
    package[3] = 0x01;
    package[4] = 0xA3;

    for(uint8_t i=0; i < packageSize; i++){
        serialport_writebyte(fd, package[i]);
    }
}

void handleArduino(uint8_t baseValue, uint8_t wristValue, uint8_t handleValue){

    uint8_t header = 0xEE;
    package[0] = header;
    if(baseValue > 0){
        package[1] = 1;
    }
    package[1] = baseValue;
    // package[1] = 90;
    package[2] = wristValue;
    // package[2] = 90;
    package[3] = handleValue;
    uint8_t checksumValue = checksum();
    package[4] = checksumValue;

    // cout << "H: " << package[0] << " S1: " << package[1] << " S2: " << package[2] << " H: " << package[3] << " CS: " << package[4] << std::endl;
    // printf("H: %X S1: %d S2: %d H: %d CS: %X \n", package[0],package[1],package[2],package[3],package[4]);

    for(uint8_t i=0; i < packageSize; i++){
        serialport_writebyte(fd, package[i]);
        // usleep(100000);
    }
}

uint8_t checksum(){
    uint8_t result = 0;
    uint16_t sum = 0;

    for(uint8_t i=0; i< (packageSize - 1);i++){
        sum += package[i];
    }

    result = sum & 0xFF;

    return result;
}

int _kbhit (void){
    struct timeval tv;
    fd_set rdfs;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rdfs);
    FD_SET (STDIN_FILENO, &rdfs);

    select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &rdfs);
}

int _getch( void ){
    int r;
    unsigned char c;
    if((r = read(0, &c, sizeof(c))) < 0 )
    {
        return r;
    }
    else
    {
        return c;
    }
}


