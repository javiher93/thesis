
#include <iostream>
#include <stdlib.h>

using namespace std;

struct Date{
    int year;
    int month;
    int day;
};

struct Book{
    char title[10];
    char author[10];
    int editionNumber;
    Date editionDate;
    double price;
};

struct Library{
    Book *pFirstBook;
    int actualSize;
    int maxSize;
};

void addBook(Library &library){

    if(library.pFirstBook == NULL){
        library.pFirstBook = new Book[2];
        library.actualSize = 0;
        library.maxSize = 2;
    } else if(library.actualSize == library.maxSize){
        int newSize = 0;
        if(library.maxSize < 16){
            newSize = library.maxSize * 2;
        } else {
            newSize = library.maxSize + 10;
        }

        Book *pNewBookArray = new Book[newSize];
        for(int i=0;i<library.maxSize;i++){
            pNewBookArray[i] = library.pFirstBook[i];
        }

        delete[] library.pFirstBook;
        library.pFirstBook = pNewBookArray;
        library.maxSize = newSize;
    }

    cout << "Write the new book" << endl;
    cout << "Title: ";
    cin >> library.pFirstBook[library.actualSize].title;
    cout << "\nAuthor: ";
    cin >> library.pFirstBook[library.actualSize].author;
    cout << "\nEdition Number: " ;
    cin >> library.pFirstBook[library.actualSize].editionNumber;
    cout <<"\n Date [DD MM YY]: ";
    cin >> library.pFirstBook[library.actualSize].editionDate.day;
    cin >> library.pFirstBook[library.actualSize].editionDate.month;
    cin >> library.pFirstBook[library.actualSize].editionDate.year;
    cout << "\nPrice: $";
    cin >> library.pFirstBook[library.actualSize].price;

    library.actualSize++;
}

void showBooks(Library &library){
    cout<< "-- Library --" <<endl;
    for(int i=0;i<library.actualSize;i++){
        cout << "\nTitle: ";
        cout <<library.pFirstBook[i].title;
        cout << "\nAuthor: ";
        cout <<library.pFirstBook[i].author;
        cout << "\n Edition Number: " ;
        cout <<library.pFirstBook[i].editionNumber;
        cout << "\nEdition Date [DD MM YY]: ";
        cout << library.pFirstBook[i].editionDate.day << " ";
        cout << library.pFirstBook[i].editionDate.month << " ";
        cout << library.pFirstBook[i].editionDate.year << " "; 
        cout << "\nPrice : $";
        cout << library.pFirstBook[i].price << endl;
        cout << "\n";
    }
}

int main(){

    Library library = {NULL, 0, 0};

    char option = '3';
    do{
        cout << "Welcome to my library" << endl;
        cout << "Choose one option" << endl;
        cout << "1.- Add book" << endl;
        cout << "2.- Show book" << endl;
        cout << "3.- Exit program" << endl;

        cin >> option;

        switch(option){
            case '1':
                addBook(library);
            break;
            case '2':
                showBooks(library);
                system("read -n 1 -s -p \"Press any key to continue...\"" );
            break;
            case '3':
                if(library.pFirstBook != NULL){
                    delete[] library.pFirstBook;
                    library.pFirstBook = NULL;
                    library.actualSize = 0;
                    library.maxSize = 0;
                }
            break;
            default:
                cout << "\nWrong option, what the heck with you!" ;
                system( "read -n 1 -s -p \"Press any key to continue...\"" );
            break;
        }

    }while(option != '3');
    
    cout << "\n\n";

    return 0;
}