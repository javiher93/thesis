
#ifndef _BANDPOWERCONTROL_H_
#define _BANDPOWERCONTROL_H_

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <fstream>
#include <sys/time.h>
#include <dirent.h>

bool parseCommand(const std::string& input, std::ostream& output);

#endif //_BANDPOWERCONTROL_H_