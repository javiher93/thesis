/*
** Copyright 2017 by Javier Heredia. All rights reserved
** Grabing bandpower data
*/

#include <mach/clock.h>
#include <mach/mach.h>
#include <iostream>
#include <string>
#include <fstream> // Consider to delete
#include <stdio.h>
#include <cstdlib>
#include <stdexcept>
#include <cassert>
#include <sstream>

// macOS Libraries
#include <unistd.h> // Consider to delete
#include <termios.h>

// Emotiv Libraries
#include "Iedk.h"
#include "IedkErrorCode.h"
#include "IEmoStateDLL.h"
#include "BandPowerControl.h"

using namespace std;

bool handleUserInput();
void promptUser();

// macOS Functions
int _kbhit(void);
int _getch(void);

int main(){

    //Initialize the handle to get the data from the headset
    EmoEngineEventHandle eEvent = IEE_EmoEngineEventCreate();
    EmoStateHandle eState = IEE_EmoStateCreate();
    unsigned int userID = 0;

    //We connect to the headset
    //If fail, end the system
    if(IEE_EngineConnect() != EDK_OK){
        cout << "Emotiv Driver start up failed.";
        return -1;
    }

    cout << "Write \"menu\" to show menu and available commands, \"exit\" to quit..." << endl;
    cout << "Band Powers Program > ";

    //Main loop
    while(true){
        
        //Record input
        if(_kbhit()){
            if(!handleUserInput()) break;
        }

        int state = IEE_EngineGetNextEvent(eEvent);
        if(state == EDK_OK){
            //Handle type of event
            IEE_Event_t eventType = IEE_EmoEngineEventGetType(eEvent);
            //Get the event from actual user
            IEE_EmoEngineEventGetUserId(eEvent, &userID);
        }
    }

    IEE_EngineDisconnect();
    IEE_EmoStateFree(eState);
    IEE_EmoEngineEventFree(eEvent);

    return 0;
}

bool handleUserInput(){
    
    static string inputBuffer;
    char key = _getch();

    if(key == '\n'){
        cout << '\n';
        string command;

        const size_t len = inputBuffer.length();
        command.reserve(len);

        //Convert the input to lower case first
        for(size_t i=0; i< len; i++){
            command.append(1, tolower(inputBuffer.at(i)));
        }

        //Clear the inputBuffer
        inputBuffer.clear();

        bool success = parseCommand(command,cout);
        promptUser();
        return success;

    } else {
        if(key == '\b'){ //Bakcspace pressed 
            if(inputBuffer.length()){
                putchar(key);
                putchar(' ');
                putchar(key);
                inputBuffer.erase(inputBuffer.end() - 1);
            }
        } else {
            //Put letter after letter in inputBuffer
            inputBuffer.append(1, key);
            cout << key;
        }
    }

    return true;
}

void promptUser(){
    cout << "Band Powers Program > ";
}

//Function to detect a key pressed, especific for macOS System
int _kbhit (void)
{
    struct timeval tv;
    fd_set rdfs;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rdfs);
    FD_SET (STDIN_FILENO, &rdfs);

    select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &rdfs);
}

int _getch(void){

    int r;
    unsigned char c;
    if((r = read(0, &c, sizeof(c))) < 0 )
    {
        return r;
    }
    else
    {
        return c;
    }
}