/* Header for using actions in the menu program
** By Javier Heredia
*/

#include "Iedk.h"
#include "IedkErrorCode.h"
#include "BandPowerControl.h"

std::string outputFile;

template <typename T>
bool convert(const std::string& str, T& value) {
	std::istringstream iss(str);
	return (iss >> value) ? true : false;
}

void split(const std::string& input, std::vector<std::string>& tokens) {

	tokens.clear();
	std::stringstream ss(input);
	std::string oneToken;
	
	while (ss >> oneToken) {
		try {
			tokens.push_back(oneToken);
		}
		catch (const std::exception&) {}
	}
}

void listFiles(const char *path){
    
    DIR *dirFile = opendir(path);
    if(dirFile){
        struct dirent* hFile;
        errno = 0;
        while((hFile = readdir(dirFile)) != NULL){
            if(!strcmp(hFile->d_name, ".")) continue;
            if(!strcmp(hFile->d_name, "..")) continue;

            // in Linux hidden files all start with '.'
            // if(gIgnoreHidden && (hFile->d_name[0] == '.')) continue;

            if(strstr(hFile->d_name, ".csv")){
                std::cout << "Found the next files: ";
                std::cout << hFile->d_name << std::endl;
            }
        }
        closedir(dirFile);
    }
}

bool parseCommand(const std::string& input, std::ostream& output){

	bool result = true;
	std::ostringstream os;
    // IEE_DataChannel_t channelList[] = {IED_AF3, IED_AF4, IED_T7, IED_T8, IED_Pz};
    IEE_DataChannel_t channelList[] = {IED_AF3};

	if (input.length()) {

		bool wrongArgument = true;
		std::vector<std::string> commands;
		split(input, commands);

		os << "==> ";

		// Quit command
		if (commands.at(0) == "exit") {
			os << "Bye!";
			result = false;
			wrongArgument = false;
		}

		// Print available commands
		else if (commands.at(0) == "menu") {
			
			os << "Available commands:" << std::endl;
			
			os << "new_user [userID] [name]";
			os << "\t\t set new user band powers" << std::endl;

            os << "see_users";
            os << "\t\t\t\t show list of users added" << std::endl;

            os << "delete_user [userID] [name.csv]";
            os << "\t\t delete CSV file from user [name.csv]" << std::endl;

            os << "record_start [userID] [name] \t\t start recording data for 60 seconds" << std::endl; 

			os << "exit \t\t\t\t\t exit this program";

			wrongArgument = false;
		}

		// New user
		else if(commands.at(0) == "new_user"){

            char cD[FILENAME_MAX];
            getcwd(cD, sizeof(cD));
            // std::string outputFile = cD;
            outputFile = cD;
            outputFile.append("/");
            //Extract the name and add the .csv extension
            std::string userName = commands.at(2);
            userName.append(".csv");
            //Add to the file 
            outputFile.append(userName);

            //Write the header for the file
            // const char header[] = "Theta, Alpha, Low_beta, High_beta, Gamma";
            // std::ofstream ofs(outputFile.c_str(), std::ios::trunc);
            // ofs(outputFile.c_str(), std::ios::trunc);
            // ofs << header << std::endl;

            unsigned int userID;
            if(convert(commands.at(1), userID)){
                wrongArgument = (IEE_FFTSetWindowingType(userID, IEE_HANNING) != EDK_OK);
            }
		}

        // See users saved
        else if(commands.at(0) == "see_users"){
            //To be implemented...

            char cD[FILENAME_MAX];
            listFiles(getcwd(cD, sizeof(cD)));

            wrongArgument = false;

        }

        else if(commands.at(0) == "delete_user"){
            //To be implemented...
            // std::string deleteFile = commands.at(2);
            // if(remove(deleteFile) != 0){
            //     std::cout << "Error deleting file" << std::endl;
            // }

            // std::cout << "Done!" << std::endl;
            wrongArgument = false;
        }

        else if(commands.at(0) == "record_start"){

            std::cout << "Recording data for user " << commands.at(2) << " ..." << std::endl;
            struct timeval t1, t2;
            double elapsedTime;

            unsigned int userID;

            convert(commands.at(1), userID);
            
            //Write header
            const char header[] = "Theta, Alpha, Low_beta, High_beta, Gamma";
            std::ofstream ofs(outputFile.c_str(), std::ios::trunc);
            ofs << header << std::endl;

            //Define bandpowers
            double alpha, low_beta, high_beta, gamma, theta;
            alpha = low_beta = high_beta = gamma = theta = 0;

            // std::ofstream ofs(outputFile.c_str(), std::ios::trunc);

            gettimeofday(&t1,NULL);
            do{
                for(int i=0;i<sizeof(channelList)/sizeof(channelList[0]);++i){
                    int result = IEE_GetAverageBandPowers(userID, channelList[i], &theta, 
                                                      &alpha, &low_beta, &high_beta, &gamma);
                    if(result == EDK_OK){
                        ofs << theta << ",";
                        ofs << alpha << ",";
                        ofs << low_beta << ",";
                        ofs << high_beta << ",";
                        ofs << gamma << ",";
                        ofs << std::endl;

                        std::cout << theta << "," << alpha << "," << low_beta << "," << std::endl;
                        std::cout << high_beta << "," << gamma << std::endl;
                    }
                }   

                gettimeofday(&t2,NULL);
                elapsedTime = (t2.tv_sec - t1.tv_sec);

            }while(elapsedTime <= 10);

            std::cout << "Done!" << std::endl;
            usleep(10000);
            wrongArgument = false;
            // Close file
            ofs.close();

        }
		
		// Unknown command
		else {
            os << "Unknown command [" << commands.at(0)
               << "]. Type \"help\" to list available commands.";
			wrongArgument = false;
		}

		if (wrongArgument) {
			os << "Wrong argument(s) for command [" << commands.at(0) << "]";
		}
	}

	const std::string& outString = os.str();
	if (outString.length()) {
		output << outString << std::endl << std::endl;
	}

	return result;
}