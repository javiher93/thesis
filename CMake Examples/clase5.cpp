#include <iostream>

using namespace std;

double factorial(int n){
    if( (n==0) || (n==1) ){
        return 1;
    } else {
        return n * factorial(n-1);
    }
}

double promedio (int *arreglo, int longitud){
    
    double promedio = 0.0;
    for(int i=0;i<longitud;i++){
        promedio += arreglo[i];
    }

    return promedio/longitud;
}

int main(){

    // for(int i=0;i<10;i++){
    //     cout << i << "! es : " << factorial(i) << endl;
    // }

    int vector[] = {1,2,3,4,5};
    const int n = sizeof(vector)/sizeof(vector[0]);
    cout << promedio(vector, n) << endl;
    return 0;
}