
#include <map>
#include <cassert>
#include "Iedk.h"
#include "IedkErrorCode.h"
#include "FacialExpressionControl.h"
#include "/include"

class _FacialExpression_ {
    //Create a dictionary key IEE_FacialExpressionAlgo_t value string
    std::map<IEE_FacialExpressionAlgo_t, std::string> _expMap;
public:
    //Class constructor
    _FacialExpression_(){
        _expMap[FE_NEUTRAL] = "neutral";
        _expMap[FE_BLINK] = "blink";
        _expMap[FE_WINK_LEF] = "wink_left";
        _expMap[FE_WINK_RIGHT] = "wink_right";
        _expMap[FE_HORIEYE] = "horieye";
        _expMap[FE_SURPRISE] = "surprise";
        _expMap[FE_FROWN] = "frown";
        _expMap[FE_SMILE] = "smile";
        _expMap[FE_CLENCH] = "clench";
    }
    
}


bool parseCommand(const std::string &command, std::ostream &output){

    bool result = true;
    std::ostringstream os;
}